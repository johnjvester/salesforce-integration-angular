import {ContactsState} from '../reducers/contact.reducer';
import {createFeatureSelector, createSelector} from '@ngrx/store';

import * as fromContact from '../reducers/contact.reducer';

export const selectContactsState = createFeatureSelector<ContactsState>("contacts");

export const selectAllContacts = createSelector(
  selectContactsState,
  fromContact.selectAll
);

export const allContactsLoaded = createSelector(
  selectContactsState,
  contactsState => contactsState.allContactsLoaded
);


