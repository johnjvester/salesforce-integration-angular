export interface Contact {
  id: string;
  Name: string;
  Title: string;
  Department: string;
}
