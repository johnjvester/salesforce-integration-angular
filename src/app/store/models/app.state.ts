import { Contact } from './contact';

export interface AppState {
  contacts: Contact[];
}
