import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Contact } from '../models/contact';
import { ContactActions, ContactActionTypes } from '../actions/contact.action';

export interface ContactsState extends EntityState<Contact> {
  allContactsLoaded: boolean;
}

export const adapter: EntityAdapter<Contact> = createEntityAdapter<Contact>();
export const initialContactsState: ContactsState = adapter.getInitialState({ allContactsLoaded: false });

export function contactReducer(state = initialContactsState, action: ContactActions): ContactsState {
  switch(action.type) {
    case ContactActionTypes.AllContactsLoaded:
      return adapter.setAll(action.payload.contacts, {...state, allContactsLoaded: true });
    case ContactActionTypes.ContactUpdated:
      return adapter.updateOne(action.payload.contact, state);
    default: {
      return state;
    }
  }
}

export const {
  selectAll
} = adapter.getSelectors();
