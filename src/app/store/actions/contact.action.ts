import { Action, createAction, props } from '@ngrx/store';
import { Contact } from '../models/contact';
import { Update } from '@ngrx/entity';

export enum ContactActionTypes {
  AllContactsRequested = '[Contact API] All Contacts Requested',
  AllContactsLoaded = '[Contact API] All Contacts Loaded',
  ContactUpdated = '[Contact API] Contact Updated'
}

export class AllContactsRequested implements Action {
  readonly type = ContactActionTypes.AllContactsRequested;
}

export class AllContactsLoaded implements Action {
  readonly type = ContactActionTypes.AllContactsLoaded;
  constructor(public payload: { contacts: Contact[] }) { }
}

export class ContactUpdated implements Action {
  readonly type = ContactActionTypes.ContactUpdated;
  constructor(public payload: { contact: Update<Contact> }) { }
}

export type ContactActions = AllContactsRequested |  AllContactsLoaded | ContactUpdated;
