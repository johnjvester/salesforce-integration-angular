import { Component, OnInit } from '@angular/core';
import { selectAllContacts } from '../store/selectors/contact.selector';
import { Contact } from '../store/models/contact';
import { Store, select } from '@ngrx/store';
import { Widget } from './widget';
import { AppState } from '../store/models/app.state';
import { ContactService } from '../contact/contact.service';
import { AllContactsLoaded } from '../store/actions/contact.action';

@Component({
  selector: 'widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})
export class WidgetComponent implements OnInit {
  loading: boolean = true;
  widget: Widget = new Widget();
  selectedContact: Contact | undefined;
  allContacts$ = this.store.pipe(select(selectAllContacts));

  constructor(
    private store: Store<AppState>,
    private contactService: ContactService
  ) {}

  ngOnInit(): void {
    this.widget.model = "Some Model Description Goes Here";

    this.contactService.getContacts().subscribe((data) => {
      this.store.dispatch(new AllContactsLoaded({ contacts: data }));
      this.loading = false;
    });
  }

  contactChange(contact:Contact):void {
    this.selectedContact = contact;
    this.widget.contactId = contact.id;
  }
}
