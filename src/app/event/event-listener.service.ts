import { Injectable, NgZone } from '@angular/core';
import { AppState } from '../store/models/app.state';
import { Action, Store } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { processSseUpdate } from './event-listener.action';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EventListenerService {
  eventSource: EventSource | undefined;

  constructor(
    private store: Store<AppState>,
    private zone: NgZone
  ) {}

  getServerSentEvent(): Observable<any> {
    return Observable.create((observer: { next: (arg0: any) => void; error: (arg0: any) => void; }) => {
      const eventSource = this.setSource();
      eventSource.onmessage = event => {
        this.zone.run(() => {
          observer.next(event);
        });
      };
      eventSource.onerror = error => {
        this.zone.run(() => {
          observer.error(error);
        });
      };
    });
  }

  register(): void {
    this.eventSource = this.setSource();
    this.eventSource.onmessage = messageEvent => this._onMessage(messageEvent);
    this.eventSource.onerror = event => this._onError(event);
  }

  private _onMessage(e: MessageEvent): void {
    const message = JSON.parse(e.data);
    if (message) {
      this.dispatchActionInNgZone(processSseUpdate(message));
    }
  }

  private _onError(e: Event): void {
    console.error('_onError', e);
  }

  private setSource(): EventSource {
    return new EventSource(
      environment.api  + '/stream/' + this.uuidv4()
    );
  }

  private dispatchActionInNgZone(action: Action): void {
    this.zone.run(() => this.store.dispatch(action));
  }

  private uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}
