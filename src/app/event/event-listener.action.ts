import { createAction, props } from '@ngrx/store';
import {Contact} from '../store/models/contact';

export const processSseUpdate = createAction(
  '[EVENT_LISTENER] Process SSE Update',
  props<{ payload:Contact }>()
);
