import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, repeat, tap } from 'rxjs/operators';
import { ContactService } from './contact.service';
import { AllContactsLoaded, AllContactsRequested, ContactActionTypes, ContactUpdated } from '../store/actions/contact.action';
import { AppState } from '../store/models/app.state';
import { Store } from '@ngrx/store';
import { EventListenerService } from '../event/event-listener.service';
import { processSseUpdate } from '../event/event-listener.action';
import { Update } from '@ngrx/entity';
import { Contact } from '../store/models/contact';

@Injectable()
export class ContactEffect {
  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private contactService: ContactService,
    private eventListenerService: EventListenerService
  ) {}

  registerEventListener$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ContactActionTypes.AllContactsLoaded),
        tap(action => {
          this.eventListenerService.register();
        }),
        repeat()
      ),
    { dispatch: false }
  );

  updateContactFromSSE$ = createEffect(() =>
    this.actions$.pipe(
      ofType(processSseUpdate),
      map( payload => {
        const anyContact:any = (payload as any);
        const contact = (anyContact as Contact);
        const updatedAction:Update<Contact> = {
          id: contact.id,
          changes: { ...contact }
        };
        return new ContactUpdated({contact: updatedAction});
      })
    )
  );

  loadAllContacts$ = this.actions$.pipe(
      ofType<AllContactsRequested>(ContactActionTypes.AllContactsRequested),
      mergeMap(() => this.contactService.getContacts()),
      map(contacts => { new AllContactsLoaded({ contacts })} )
    );
}
