import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { ContactService } from './contact/contact.service';
import { ContactEffect } from './contact/contact.effect';
import { contactReducer } from './store/reducers/contact.reducer';
import { WidgetComponent } from './widget/widget.component';
import { EventListenerService } from './event/event-listener.service';

@NgModule({
  declarations: [
    AppComponent,
    WidgetComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot([]),
    EffectsModule.forRoot([ContactEffect]),
    StoreModule.forFeature('contacts', contactReducer)
  ],
  providers: [
    ContactService,
    EventListenerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
